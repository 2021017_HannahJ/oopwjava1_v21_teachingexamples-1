package oojava1.m04.ex06_exception;

public class ArrayExceptionDemo {

	public static void main(String[] args) {
		int[] a = new int[7];
		for (int i=0;i<7;i++)
			a[i]=i*i;
		// Print
		for (int i=0;i<10;i++) {
			try {
				System.out.printf("Value of element %d is %d%n",i,a[i]);
			}
			catch (ArrayIndexOutOfBoundsException e) {
				System.out.printf("Value of element %d is %s%n",i,e);
			} // try-catch
		}//for
	}
} //class
